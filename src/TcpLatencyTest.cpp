#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <bits/stdc++.h>

#define FTRACE( fd, PREFIX, ... )                                                                                                                   \
    do                                                                                                                                              \
    {                                                                                                                                               \
        fprintf( fd, "%s %s [%s:%d] ", print_time(), PREFIX, __FILE__, __LINE__ );                                                                  \
        fprintf( fd, __VA_ARGS__ );                                                                                                                 \
        fprintf( fd, "\n" );                                                                                                                        \
    } while ( false )

#define TRACE( ... ) FTRACE( stdout, "INFO", __VA_ARGS__ )
#define TRACE_E( ... ) FTRACE( stdout, "ERROR", __VA_ARGS__ )

#ifdef NDEBUG
#define DBG_TRACE( ... ) while ( false )
#else
#define DBG_TRACE( ... ) TRACE( __VA_ARGS__ )
#endif

#define ASSERT( expr )                                                                                                                              \
    do                                                                                                                                              \
    {                                                                                                                                               \
        if ( !( expr ) )                                                                                                                            \
        {                                                                                                                                           \
            TRACE_E( "ASSERTION failed : " #expr );                                                                                                 \
            abort();                                                                                                                                \
        }                                                                                                                                           \
    } while ( false )

/// Usage: ASSERT_OP(==, x, y, << "this is an error"); ASSERT_OP(==, x, y);
#define ASSERT_OP( op, x, y, ... )                                                                                                                  \
    do                                                                                                                                              \
    {                                                                                                                                               \
        auto x_ = x;                                                                                                                                \
        auto y_ = y;                                                                                                                                \
        if ( !( x_ op y_ ) )                                                                                                                        \
        {                                                                                                                                           \
            std::stringstream ss;                                                                                                                   \
            ss << x << " " << #op << " " << y << ". " __VA_ARGS__;                                                                                  \
            TRACE_E( "ASSERTION failed. Expected %s %s %s, but got %s", #x, #op, #y, ss.str().c_str() );                                            \
            abort();                                                                                                                                \
        }                                                                                                                                           \
    } while ( false )

#define ASSERT_EQ( x, y, ... ) ASSERT_OP( ==, x, y, __VA_ARGS__ )
#define ASSERT_LE( x, y, ... ) ASSERT_OP( <=, x, y, __VA_ARGS__ )


/// Print timestamp.
/// @param time if ts.tv_nsec == -1, get local time.
/// @param buffer [out] if it's nullptr, use internal static buffer.
inline const char *print_time( char *buffer = nullptr, timespec ts = { -1, -1 }, bool bUTCTime = false )
{
    static const unsigned BUFFERSIZE = 32;
    static char           localbuf[BUFFERSIZE];
    const char           *fmt = "%Y%m%d-%T";

    char *buf = buffer ? buffer : localbuf;
    if ( ts.tv_nsec == -1 )
        clock_gettime( CLOCK_REALTIME, &ts );
    time_t timet = ts.tv_sec;
    tm     atime;
    bUTCTime ? gmtime_r( &timet, &atime ) : localtime_r( &timet, &atime );
    size_t nbytes = strftime( buf, BUFFERSIZE, fmt, &atime );   // YYYYMMDD-HH:MM:SS
    sprintf( buf + nbytes, ".%06d", int( ts.tv_nsec / 1000 ) ); // us
    return buf;
}

struct SockProcessor
{
    int          m_sock;
    virtual void processIn() = 0;
    // virtual void processOut() = 0; // not surpported right now.
    // SockProcessor will be removed from EpollManager after processClose is called.
    virtual void processClose()
    {
        TRACE( "processClose on sock: %d", m_sock );
    }
    // SockProcessor will be removed from EpollManager after processError is called.
    virtual void processError()
    {
        TRACE( "processError on sock: %d", m_sock );
    }

    virtual ~SockProcessor()
    {
    }
};
using SockProcessorPtr = std::unique_ptr<SockProcessor>;

inline timespec now_timespec()
{
    timespec ts;
    if ( 0 != clock_gettime( CLOCK_REALTIME, &ts ) )
    {
        TRACE_E( "Failed to call clock_gettime" );
        exit( 1 );
    }
    return ts;
}
inline int64_t to_nanosec( const timespec &ts )
{
    return ts.tv_sec * 1000000000 + ts.tv_nsec;
}

struct EpollManager
{
    int m_epollFd = 0;

    std::atomic<int> m_stopping{ 2 }; // 0: running, 1: stoping, 2: stopped

    static constexpr size_t MAX_EVENTS = 64;
    epoll_event             events[MAX_EVENTS];

    std::vector<SockProcessorPtr> m_sockProcessors;
    int                           polling_timeout_millisec = 10;

    EpollManager()
    {
        m_sockProcessors.resize( 1024 );
    }

    ~EpollManager()
    {
        close_socks();
    }

    bool init()
    {
        if ( ( m_epollFd = epoll_create1( 0 ) ) <= 0 )
        {
            TRACE_E( "Failed to create epoll fd" );
            return false;
        }
        m_stopping = 0;
        return true;
    }

    // return number of events recved, or -1 for error.
    int recv_sock_events()
    {
        int nEvents = epoll_wait( m_epollFd, events, MAX_EVENTS,
                                  polling_timeout_millisec ); // polling_timeout_millisec.load() );
        if ( nEvents == -1 )
        {
            TRACE_E( "ERROR epoll_wait error: %d ", errno );
            return -1;
        }
        for ( int i = 0; i < nEvents; ++i )
        {
            assert( events[i].data.fd );
            auto evts = events[i].events;
            if ( !events[i].data.fd )
            {
                TRACE_E( "null EpollUserData on sock" );
                return -1;
            }
            assert( m_sockProcessors.at( events[i].data.fd ) );
            SockProcessorPtr &proc = m_sockProcessors.at( events[i].data.fd );
            if ( !proc )
            {
                TRACE_E( " no sock processor for sock: %d ", events[i].data.fd );
                return -1;
            }

            if ( evts & EPOLLIN )
            {
                proc->processIn();
            }
            if ( ( evts & EPOLLHUP ) || ( evts & EPOLLRDHUP ) )
            {
                proc->processClose();
                close_sock( events[i].data.fd );
            }
            if ( evts & EPOLLERR )
            {
                proc->processError();
                close_sock( events[i].data.fd );
            }
        }
        return nEvents;
    }

    void stop()
    {
        m_stopping = 1;
    }

    bool stopped()
    {
        return m_stopping == 2;
    }

    void close_socks()
    {
        if ( m_stopping == 2 ) // stopped already.
            return;
        m_stopping = 2;
        TRACE( "Closing all socks" );
        for ( size_t i = 0; i < m_sockProcessors.size(); ++i )
        {
            if ( m_sockProcessors[i] && m_sockProcessors[i]->m_sock > 0 )
            {
                close( m_sockProcessors[i]->m_sock );
            }
            m_sockProcessors[i] = nullptr;
        }
        close( m_epollFd );
        m_epollFd = 0;
    }

    void loop_events_until_stop()
    {
        while ( !m_stopping )
        {
            if ( recv_sock_events() == -1 )
            {
                close_socks();
                return;
            }
        }
        close_socks();
    }

    /// return 0 or errno.
    int add_sock( SockProcessorPtr proc )
    {
        assert( proc );
        assert( proc->m_sock > 0 );
        assert( proc->m_sock < 1024 );
        assert( !m_sockProcessors.at( proc->m_sock ) );

        int        sock = proc->m_sock;
        epoll_data data;
        data.fd                 = sock;
        uint32_t    epollevents = EPOLLIN | EPOLLET;
        epoll_event evt;
        evt.events = epollevents;
        evt.data   = data;

        int err = epoll_ctl( m_epollFd, EPOLL_CTL_ADD, sock, &evt ) == 0 ? 0 : errno;
        if ( err )
        {
            TRACE_E( "Failed to epoll_ctl EPOLL_CTL_ADD on sock: %d", sock );
            return err;
        }

        m_sockProcessors[sock] = std::move( proc );
        TRACE( "Added sock: %d", sock );
        return 0;
    }
    void close_sock( int sock )
    {
        SockProcessorPtr &proc = m_sockProcessors.at( sock );
        if ( proc )
        {
            if ( epoll_ctl( m_epollFd, EPOLL_CTL_DEL, sock, nullptr ) == 0 )
            {
                close( sock );
                m_sockProcessors.at( sock ).reset();
                TRACE( "Close sock: %d", sock );
            }
        }
    }
};

/// Nonblocking recv message with max bufLen size until EAGAIN or EWOULDBLOCK. MSG_DONTWAIT is used as flag to force a non-blocking read.
/// \return <errcode, bytesRead>. errcode == 0 when internal recv returns EAGAIN or EWOULDBLOCK.
/// errcode == -1 to indicate remote closed.
inline std::pair<int, unsigned> recvNonBlocking( int sockid, char *buf, size_t bufLen )
{
    if ( !bufLen )
        return { 0, 0u };

    unsigned bytesReceived = 0;
    while ( bufLen )
    {
        ssize_t recvRet = ::recv( sockid, &buf[bytesReceived], bufLen, MSG_DONTWAIT );
        if ( recvRet > 0 )
        {
            bytesReceived += recvRet;
            bufLen -= recvRet;
        }
        else if ( recvRet == 0 ) // remote closed.
        {
            return { -1, bytesReceived };
        }
        else if ( recvRet == -1 )
        {
            int err = errno;
            if ( err == EAGAIN && err == EWOULDBLOCK )
                return { 0, bytesReceived };
            else
                return { err, bytesReceived };
        }
        else
        {
            assert( false && "Unexpected negative ret!" );
            return { -1, bytesReceived }; // in case of reaching here.
        }
    }
    return { 0, bytesReceived };
}

void setTCPNoDelay( int sock )
{
    int yes = 1;
    if ( setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char *)&yes, sizeof( int ) ) < 0 )
    {
        TRACE_E( "Faield to set TCP_NODELAY for sock: %d", sock );
    }
}
void setTCPQuickAck( int sock )
{
    int yes = 1;
    if ( setsockopt( sock, IPPROTO_TCP, TCP_QUICKACK, (char *)&yes, sizeof( int ) ) < 0 )
    {
        TRACE_E( "Faield to set TCP_QUICKACK for sock: %d", sock );
    }
}

/// connect to remote and return sock fd.
inline int sock_connect( int port, std::string aip )
{
    int         sock;
    sockaddr_in serv_addr;
    if ( ( sock = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
    {
        TRACE_E( "Socket creation error" );
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port   = htons( port );
    std::string ip       = aip.empty() ? "127.0.0.1" : aip.c_str();

    // Convert IPv4 and IPv6 addresses from text to binary form
    if ( inet_pton( AF_INET, ip.c_str(), &serv_addr.sin_addr ) <= 0 )
    {
        TRACE_E( "Invalid address/ Address not supported: %s", ip.c_str() );
        close( sock );
        return -1;
    }

    if ( connect( sock, (struct sockaddr *)&serv_addr, sizeof( serv_addr ) ) < 0 )
    {
        TRACE_E( "Connection Failed: %s:%d", ip.c_str(), port );
        close( sock );
        return -1;
    }
    setTCPNoDelay( sock );
    setTCPQuickAck( sock );
    return sock;
}

/// bind on port and return sock.
inline int sock_listen_on( const std::string &port )
{
    struct addrinfo hints;

    std::memset( &hints, 0, sizeof( struct addrinfo ) );
    hints.ai_family   = AF_UNSPEC;   /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* TCP */
    hints.ai_flags    = AI_PASSIVE;  /* All interfaces */

    struct addrinfo *result;
    int              sockt = getaddrinfo( nullptr, port.c_str(), &hints, &result );
    if ( sockt != 0 )
    {
        TRACE_E( "getaddrinfo failed, port: %s", port.c_str() );
        return -1;
    }

    struct addrinfo *rp;
    int              socketfd;
    for ( rp = result; rp != nullptr; rp = rp->ai_next )
    {
        socketfd = socket( rp->ai_family, rp->ai_socktype, rp->ai_protocol );
        if ( socketfd == -1 )
        {
            continue;
        }

        sockt = bind( socketfd, rp->ai_addr, rp->ai_addrlen );
        if ( sockt == 0 )
        {
            break;
        }

        close( socketfd );
    }

    if ( rp == nullptr )
    {
        TRACE_E( "bind failed port: %s, err: %d %s", port.c_str(), errno, strerror( errno ) );
        socketfd = -1;
    }

    freeaddrinfo( result );

    if ( listen( socketfd, SOMAXCONN ) == -1 )
    {
        TRACE_E( "listen failed on port: %s", port.c_str() );
        return 1;
    }

    return socketfd;
}

struct ServerSockProcessor : SockProcessor
{
    /// return false to reject & close new sock.
    bool onNewConnection( SockProcessor &listenSock, int newSock, int peerPort, const std::string &peerHost )
    {
        return true;
    }
};

/// ServerSockProcessorT: inherites ServerSockProcessor
//       - defines type InitParam which has member epollMgr.
//       - has member init(const InitParam&).
template<class ServerSockProcessorT>
struct ListenServerBase : SockProcessor
{
    using ServerInitParam = typename ServerSockProcessorT::InitParam;

    ServerInitParam m_param;

    void init( const ServerInitParam &param )
    {
        m_param = param;
    }

    void startServe( int port )
    {
        m_sock = sock_listen_on( std::to_string( port ) );
        if ( m_sock <= 0 )
            exit( 1 );
        TRACE( "Create listen sock %d on port %d", m_sock, port );
    }

    // new connection
    void processIn() override
    {
        struct sockaddr in_addr;
        socklen_t       in_len = sizeof( in_addr );
        int             infd   = accept( m_sock, &in_addr, &in_len );
        if ( infd == -1 )
        {
            if ( errno == EAGAIN || errno == EWOULDBLOCK ) // Done processing incoming connections
            {
                TRACE_E( "accept on sock: %d", m_sock );
                m_param.epollMgr->stop();
                return;
            }
            else
            {
                TRACE_E( "accept failed sock:%d", m_sock );
                m_param.epollMgr->stop();
                return;
            }
        }

        std::string hbuf( NI_MAXHOST, '\0' ); // ip
        std::string sbuf( NI_MAXSERV, '\0' ); // port
        int         ret = getnameinfo( &in_addr,
                               in_len,
                               const_cast<char *>( hbuf.data() ),
                               hbuf.size(),
                               const_cast<char *>( sbuf.data() ),
                               sbuf.size(),
                               NI_NUMERICHOST | NI_NUMERICSERV );

        auto proc    = std::make_unique<ServerSockProcessorT>();
        proc->m_sock = infd;
        proc->init( m_param );

        if ( proc->onNewConnection( *this, infd, atoi( sbuf.c_str() ), hbuf.c_str() ) )
        {
            TRACE( "Accepted remote connection sock %d, from %s:%s on listen sock: %d ", infd, hbuf.c_str(), sbuf.c_str(), m_sock );
        }
        else
        {
            TRACE( "Rejected remote connection sock %d, from %s:%s on listen sock: %d ", infd, hbuf.c_str(), sbuf.c_str(), m_sock );
            close( infd );
            return;
        }
        setTCPNoDelay( infd );
        setTCPQuickAck( infd );

        m_param.epollMgr->add_sock( std::move( proc ) );
    }
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
//                Server & Client
///////////////////////////////////////////////////////////////////////////////////////////////////////

struct MsgBuffer
{
    static constexpr int64_t FIN_SEQ = -1;

    struct MsgHeader
    {
        int64_t  seq;
        timespec sendTime;
        unsigned payloadLen;
        char     payload[];

        unsigned getLength() const
        {
            return sizeof( MsgHeader ) + payloadLen;
        }
    };
    std::vector<char> m_buffer;

    static constexpr size_t MSGHEADER_SIZE = sizeof( MsgHeader );

    void resize( unsigned nbytes )
    {
        m_buffer.resize( nbytes );
    }
    MsgHeader &getMsg()
    {
        return *reinterpret_cast<MsgHeader *>( &m_buffer[0] );
    }
    char *getBuffer()
    {
        return &m_buffer[0];
    }
};

struct InitClientParam
{
    EpollManager *epollMgr;
    int           nMsgs;
    int           nMsgSize;
    int           bRunForever;
};

#define PRTSTATS( name, arr ) printf( name " \t %.2f \t %.2f \t %.2f \t %.2f \n", arr[0], arr[N / 2], arr[N * 0.9], arr[N - 2] )

#define PRT_HEAD( ... )                                                                                                                             \
    printf( "\n====================================================================\n" );                                                           \
    printf( __VA_ARGS__ );                                                                                                                          \
    printf( "                \t   Min \t 50%% \t 90%% \t Max \n" )

struct TimeStats
{
    std::vector<int64_t>                sendBeginTs, sendEndTs, recvBeginTs, recvEndTs, peerSendTs;
    std::vector<std::vector<int64_t> *> vecOfTimes;

    void reserve( unsigned n )
    {
        vecOfTimes = std::vector<std::vector<int64_t> *>{ &sendBeginTs, &sendEndTs, &recvBeginTs, &recvEndTs, &peerSendTs };
        for ( auto pTimes : vecOfTimes )
            pTimes->reserve( n );
    }
    void clear()
    {
        for ( auto pTimes : vecOfTimes )
            pTimes->clear();
    }
};


struct EchoServer : ServerSockProcessor
{
    // requried by ListenServerBase
    using InitParam = InitClientParam;

    MsgBuffer m_msgTmpl;

    InitParam m_params;
    int       m_nRecvs;

    TimeStats m_times;

    void init( const InitParam &param )
    {
        m_params = param;
        m_msgTmpl.resize( param.nMsgSize );
        m_times.reserve( param.nMsgs );
    }

    void processIn() override
    {
        timespec t0 = now_timespec();
        // int      nbytes = ::recv( m_sock, m_msgTmpl.getBuffer(), m_params.nMsgSize, 0 );
        // if ( nbytes == 0 )  // peer close
        // {
        //     m_params.epollMgr->close_sock( m_sock );
        //     return;
        // }
        auto errAndBytes = recvNonBlocking( m_sock, m_msgTmpl.getBuffer(), m_params.nMsgSize );
        if ( errAndBytes.first == -1 ) // peer close
        {
            m_params.epollMgr->close_sock( m_sock );
            return;
        }
        assert( errAndBytes.first == 0 );
        assert( errAndBytes.second == m_msgTmpl.getMsg().getLength() );

        timespec t1                 = now_timespec();
        timespec peerSendTime       = m_msgTmpl.getMsg().sendTime;
        m_msgTmpl.getMsg().sendTime = t1;

        ::send( m_sock, m_msgTmpl.getBuffer(), m_params.nMsgSize, 0 );
        timespec t2 = now_timespec();

        m_times.recvBeginTs.push_back( to_nanosec( t0 ) );
        m_times.recvEndTs.push_back( to_nanosec( t1 ) );

        m_times.sendBeginTs.push_back( to_nanosec( t1 ) );
        m_times.sendEndTs.push_back( to_nanosec( t2 ) );
        m_times.peerSendTs.push_back( to_nanosec( peerSendTime ) );

        ++m_nRecvs;
        if ( m_nRecvs && ( m_nRecvs % m_params.nMsgs == 0 ) )
        {
            printStats();
            m_times.clear();
        }
        // server never closes socket.
    }

    void printStats()
    {
        const unsigned      N = m_params.nMsgs;
        std::vector<double> sendTimes( N ), recvTimes( N ), stTimes( N ); // in us
        for ( size_t i = 0; i < N; ++i )
        {
            sendTimes[i] = ( m_times.sendEndTs[i] - m_times.sendBeginTs[i] ) / 1000.0;
            recvTimes[i] = ( m_times.recvEndTs[i] - m_times.recvBeginTs[i] ) / 1000.0;
            stTimes[i]   = ( m_times.recvEndTs[i] - m_times.peerSendTs[i] ) / 1000.0;
        }
        std::sort( sendTimes.begin(), sendTimes.end() );
        std::sort( recvTimes.begin(), recvTimes.end() );
        std::sort( stTimes.begin(), stTimes.end() );

        PRT_HEAD( "    TCP Server Latencies (usec) with msgsize: %d, nMsgs: %d\n", m_params.nMsgSize, N );
        PRTSTATS( "sendTime       ", sendTimes );
        PRTSTATS( "recvTime       ", recvTimes );
        PRTSTATS( "SingleTripTime ", stTimes );
        printf( "\n" );
    }
};


/// Client sends msg and wait for echo msg.
struct Client : SockProcessor
{
    int64_t   m_nextSeq = 0;
    MsgBuffer m_msgTmpl;

    InitClientParam m_params;

    TimeStats m_times;

    void init( const InitClientParam &param )
    {
        m_params = param;
        m_msgTmpl.resize( m_params.nMsgSize );

        m_times.reserve( m_params.nMsgs );
    }

    void connect( int port, std::string ip = "127.0.0.1" )
    {
        // connect to local port

        m_sock = sock_connect( port, ip );
        if ( m_sock <= 0 )
            exit( 1 );

        TRACE( "create client sock %d connecting to %s:%d", m_sock, ip.c_str(), port );
    }

    void sendFistMsg()
    {
        sendMsg( m_nextSeq++ );
    }
    void sendMsg( int64_t seq )
    {
        m_msgTmpl.getMsg().seq        = seq;
        m_msgTmpl.getMsg().payloadLen = m_params.nMsgSize - m_msgTmpl.MSGHEADER_SIZE;
        m_msgTmpl.getMsg().sendTime   = now_timespec();

        ::send( m_sock, m_msgTmpl.getBuffer(), m_params.nMsgSize, 0 );

        m_times.sendEndTs.push_back( to_nanosec( now_timespec() ) );
        m_times.sendBeginTs.push_back( to_nanosec( m_msgTmpl.getMsg().sendTime ) );
    }

    void processIn() override
    {
        timespec t0 = now_timespec();
        // ::recv( m_sock, m_msgTmpl.getBuffer(), m_params.nMsgSize, 0 );
        auto     errAndBytes = recvNonBlocking( m_sock, m_msgTmpl.getBuffer(), m_params.nMsgSize );
        timespec t1          = now_timespec();

        if ( errAndBytes.first == -1 ) // peer close
        {
            m_params.epollMgr->close_sock( m_sock );
            return;
        }
        assert( errAndBytes.first == 0 );
        assert( errAndBytes.second == m_msgTmpl.getMsg().getLength() );

        m_times.recvBeginTs.push_back( to_nanosec( t0 ) );
        m_times.recvEndTs.push_back( to_nanosec( t1 ) );
        m_times.peerSendTs.push_back( to_nanosec( m_msgTmpl.getMsg().sendTime ) );

        if ( m_nextSeq && ( m_nextSeq % m_params.nMsgs == 0 ) )
        {
            printStats();
            m_times.clear();
        }
        if ( m_nextSeq == m_params.nMsgs && !m_params.bRunForever )
        {
            m_params.epollMgr->stop();
        }
        else
        {
            sendMsg( m_nextSeq++ );
        }
    }

    void printStats()
    {
        const unsigned      N = m_params.nMsgs;
        std::vector<double> sendTimes( N ), recvTimes( N ), stTimes( N ), rtTimes( N ); // in us
        for ( size_t i = 0; i < N; ++i )
        {
            sendTimes[i] = ( m_times.sendEndTs[i] - m_times.sendBeginTs[i] ) / 1000.0;
            recvTimes[i] = ( m_times.recvEndTs[i] - m_times.recvBeginTs[i] ) / 1000.0;
            stTimes[i]   = ( m_times.recvEndTs[i] - m_times.peerSendTs[i] ) / 1000.0;
            rtTimes[i]   = ( m_times.recvEndTs[i] - m_times.sendBeginTs[i] ) / 1000.0;
        }
        std::sort( sendTimes.begin(), sendTimes.end() );
        std::sort( recvTimes.begin(), recvTimes.end() );
        std::sort( stTimes.begin(), stTimes.end() );
        std::sort( rtTimes.begin(), rtTimes.end() );

        PRT_HEAD( "    TCP Client Latencies (usec) with msgsize: %d, nMsgs: %d\n", m_params.nMsgSize, N );
        PRTSTATS( "sendTime        ", sendTimes );
        PRTSTATS( "recvTime        ", recvTimes );
        PRTSTATS( "SingleTripTime  ", stTimes );
        PRTSTATS( "RoundTripTime   ", rtTimes );
        printf( "\n" );
    }
};

EpollManager g_epollMgr;

void handle_sig( int sig )
{
    g_epollMgr.close_socks();
}

int main( int argn, const char *argv[] )
{
    auto usage = []( std::string errmsg )
    {
        if ( !errmsg.empty() )
        {
            std::cerr << errmsg << "\n";
        }
        std::cout << "\nTCPLatency [server|client|both] [-p port] [-H host] [-n nMsgs] [-m MsgSize]\n"
                  << "    server|client    Runs as server or client. If not specified, "
                     "it runs as both.\n"
                  << "    -h               Show this help message\n"
                  << "    -p port          Port number, 12323 by default.\n"
                  << "    -H host          Host IP/name to connect. Applicable for client only, default to localhost.\n"
                  << "    -n nMsgs         Number of messages to send. 1000 by default.\n"
                  << "    -m MsgSize       Message size in bytes, default 64.\n"
                  << "    -f               Run forever until input Ctl+C.\n";
        exit( errmsg.empty() ? 0 : 1 );
    };

    int         port             = 12323;
    std::string host             = "127.0.0.1";
    int         nMsgs            = 1000;
    int         nMsgSize         = 64;
    int         asServerOrClient = 3; // 1: server, 2: client, 3: both;
    int         bRunForever      = 0;

    for ( int i = 1; i < argn; ++i )
    {
        if ( strcmp( argv[i], "server" ) == 0 )
        {
            asServerOrClient = 1;
        }
        else if ( strcmp( argv[i], "client" ) == 0 )
        {
            asServerOrClient = 2;
        }
        else if ( strcmp( argv[i], "both" ) == 0 )
        {
            asServerOrClient = 3;
        }
        else if ( strcmp( argv[i], "-f" ) == 0 )
        {
            bRunForever = 1;
        }
        else if ( strcmp( argv[i], "-p" ) == 0 )
        {
            port = atoi( argv[++i] );
        }
        else if ( strcmp( argv[i], "-H" ) == 0 )
        {
            host = argv[++i];
        }
        else if ( strcmp( argv[i], "-n" ) == 0 )
        {
            nMsgs = atoi( argv[++i] );
        }
        else if ( strcmp( argv[i], "-m" ) == 0 )
        {
            nMsgSize = atoi( argv[++i] );
        }
        else if ( strcmp( argv[i], "-h" ) == 0 )
        {
            usage( "" );
        }
        else
        {
            usage( "Unknown argument: " + std::string( argv[i] ) );
        }
    }

    TRACE( "Starting as serverOrClient:%d", asServerOrClient );

    signal( SIGINT, handle_sig );

    if ( !g_epollMgr.init() )
        exit( 1 );
    InitClientParam param{ &g_epollMgr, nMsgs, nMsgSize, bRunForever };
    if ( asServerOrClient & 0x1 ) // server
    {
        auto server = std::make_unique<ListenServerBase<EchoServer>>();
        server->init( param );

        server->startServe( port );
        g_epollMgr.add_sock( std::move( server ) );
    }
    if ( asServerOrClient & 0x2 ) // client
    {
        auto client = std::make_unique<Client>();
        client->init( param );
        client->connect( port, host );
        client->sendFistMsg();
        g_epollMgr.add_sock( std::move( client ) );
    }
    g_epollMgr.loop_events_until_stop();
    TRACE( " The END." );
}
