#include "JzLogger.h"

int main( int argc, const char *argv[] )
{
    using IntVec = std::vector<int>;
    jz::ProcOStream::getGlobalProcOut( &std::cout ); // override out stream
    std::vector<std::tuple<std::string, int>> vec = { { "k1", 2 }, { "k2", 3 } };

    std::map<std::string, std::shared_ptr<IntVec>> amap = { { "k5", std::make_shared<IntVec>( IntVec{ 2, 3, 4 } ) },
                                                            { "k2", std::make_shared<IntVec>( IntVec{ 3, 5, 2 } ) } };

    JZLOGKV( "test kv output", "key1", 23, "k2", "saf", 3, 23, "vecOfTuple", vec );
    JZLOGB( "braces format log key1={}, vecofTuple={}, amap={}", 23, vec, amap ); // auto deref pointers.

    jz::getGlobalDelimiters().quoteStrElem = true;
    JZLOGB( "------------ quote string type of elements in a container -------" );
    JZLOGKV( "test kv output", "key1", 23, "k2", "saf", 3, 23, "vecOfTuple", vec );
    JZLOGB( "braces format log {}={}, vecofTuple={}, amap={}", "key1", 23, vec, amap );
    return 0;
}