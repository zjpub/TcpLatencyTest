#pragma once
#include <utility>
#include <assert.h>
#include <time.h>
#include <ostream>
#include <sstream>
#include <iostream>
#include <fstream>
#include <memory>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <optional>

#include <unistd.h> // getpid

/// Usage:
///    JZLOGB("by default, log info message pid={}", getpid()); // braces represents parameter to be printed with ostream << var.
///    JZLOGKV("log rest arguments as key-value pairs", "key1", val1, key2, val2); // key/value could be of any printable types.
///    JZLOGB(myostream, "User may provide an ostream instead of using global ofstream which writes to file <Program>-pid<pid>.log").
///    DBG_TRACE("debug level  messages are printed ony when built when NDEBUG is not defined");
///    SetLogLevelThreshold('E'); // It sets the global log level/category threshold, the letter is the first letter of level/category: Error, Warn, Info, Trace, Debug.
///                               // Note: Debug msg will always be logged in debug build, but not logged in release build.
///    LogErrorCounter(); // to get and reset error counter.
///  Users have 2 methods to override default log ostream:
///    - define their own std::ostream by defining JZLOG_TO_STREAM.
///    - call jz::ProcOStream::getGlobalProcOut(&newOstream) to override it.
///  c++ std containers will be auto printed. Use jz::getGlobalDelimiters() to set delimiters.

//#define JZLOGB_( CATEGORY, ... ) do{}while(false)  // uncomment this line to disable all JZLOG macros.

#define JZLOGB_( CATEGORY, ... )                                                                                                                    \
    if ( jz::canlog( CATEGORY[0] ) )                                                                                                                \
    jz::jzlogB( CATEGORY, __VA_ARGS__ )

#define JZLOGKV_( CATEGORY, ... )                                                                                                                   \
    if ( jz::canlog( CATEGORY[0] ) )                                                                                                                \
    jz::jzlogKV( CATEGORY, __VA_ARGS__ )

#ifndef JZLOG_TO_STREAM
//#define JZLOG_TO_STREAM  jz::ProcOStream::getGlobalProcOut()  // default value
//#define JZLOG_TO_STREAM  std::cout
#define JZLOG_TO_STREAM std::cerr
#endif

#define JZLOGB_E( ... ) JZLOGB_( "E", __FILE__, __LINE__, __VA_ARGS__ )
#define JZLOGB_W( ... ) JZLOGB_( "W", __FILE__, __LINE__, __VA_ARGS__ )
#define JZLOGB( ... ) JZLOGB_( "I", __FILE__, __LINE__, __VA_ARGS__ )
#define JZLOGB_T( ... ) JZLOGB_( "T", __FILE__, __LINE__, __VA_ARGS__ )

#define JZLOGKV_E( ... ) JZLOGKV_( "E", __FILE__, __LINE__, __VA_ARGS__ )
#define JZLOGKV_W( ... ) JZLOGKV_( "W", __FILE__, __LINE__, __VA_ARGS__ )
#define JZLOGKV( ... ) JZLOGKV_( "I", __FILE__, __LINE__, __VA_ARGS__ )
#define JZLOGKV_T( ... ) JZLOGKV_( "T", __FILE__, __LINE__, __VA_ARGS__ )

#ifdef NDEBUG
#define DBG_LOGB( ... )                                                                                                                             \
    do                                                                                                                                              \
    {                                                                                                                                               \
    } while ( false )
#define DBG_LOGKB( ... ) DBG_LOGB( __VA_ARGS__ )
#else
#define DBG_LOGB( ... ) JZLOGB_( "D", __FILE__, __LINE__, __VA_ARGS__ )
#define DBG_LOGKV( ... ) JZLOGKV_( "D", __FILE__, __LINE__, __VA_ARGS__ )
#endif

namespace jz
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//   print containers: std::vector, std::array, std::set, std::map, set::unordered_set, set::unordered_map
//////////////////////////////////////////////////////////////////////////////////////////////////////////

struct Delimiters
{
    std::string elemDelim    = ", ";
    std::string pairDelim    = ": ";
    std::string tupleDelim   = ", ";
    std::string vecQuotes    = "[]";  // 2 chars or null
    std::string mapQuotes    = "{}";  // 2 chars or null
    std::string tupleQuotes  = "()";  // 2 chars or null
    bool        quoteStrElem = false; // quote key, value, element in a map/vector.
};

inline Delimiters &getGlobalDelimiters()
{
    static thread_local Delimiters s_d;
    return s_d;
}

//============ IsLikeStr<T> ============
template<class T>
struct IsLikeStr : std::false_type
{
};
template<class... T>
struct IsLikeStr<std::basic_string<T...>> : std::true_type
{
};
template<class... T>
struct IsLikeStr<std::basic_string_view<T...>> : std::true_type
{
};
template<>
struct IsLikeStr<const char *> : std::true_type
{
};
template<>
struct IsLikeStr<char *> : std::true_type
{
};
template<size_t N>
struct IsLikeStr<char[N]> : std::true_type
{
};
template<size_t N>
struct IsLikeStr<const char[N]> : std::true_type
{
};


template<class T>
static constexpr bool IsLikeStr_v = IsLikeStr<std::remove_cv_t<std::remove_reference_t<T>>>::value;

//============ IsDeref<T> ============
template<class T>
struct IsDeref : std::false_type
{
};
template<class... T>
struct IsDeref<std::unique_ptr<T...>> : std::true_type
{
};
template<class... T>
struct IsDeref<std::shared_ptr<T...>> : std::true_type
{
};
template<class T>
struct IsDeref<std::optional<T>> : std::true_type
{
};
template<class T>
struct IsDeref<T *> : std::true_type
{
};

template<class T>
static constexpr bool IsDeref_v = IsDeref<std::remove_cv_t<std::remove_reference_t<T>>>::value;

template<class T>
std::ostream &print( std::ostream &oss, const T &val )
{
    if constexpr ( IsLikeStr_v<T> )
    {
        if ( getGlobalDelimiters().quoteStrElem )
            oss << '\"';
    }
    if constexpr ( IsDeref_v<T> && !IsLikeStr_v<T> )
    {
        oss << *val; // auto deref
    }
    else
    {
        oss << val;
    }
    if constexpr ( IsLikeStr_v<T> )
    {
        if ( getGlobalDelimiters().quoteStrElem )
            oss << '\"';
    }
    return oss;
}

template<class K, class V>
std::ostream &printPair( std::ostream &oss, const std::pair<K, V> &val )
{
    print( oss, val.first ) << getGlobalDelimiters().pairDelim;
    print( oss, val.second );
    return oss;
}

template<bool bQuoteKeyIfStr = false, bool bQuoteValIfStr = false, size_t idx = 0, class K, class V, class... Args>
std::ostream &printPairs( const std::string &delim, const std::string &kvDelim, std::ostream &oss, K &&k, V &&v, Args &&...args )
{
    static_assert( ( sizeof...( Args ) % 2 ) == 0, "Even number of args." );
    if ( idx > 0 )
        oss << delim;
    print( oss, k ) << kvDelim;
    print( oss, v );
    if constexpr ( sizeof...( Args ) != 0 )
    {
        return printPairs<bQuoteKeyIfStr, bQuoteValIfStr, idx + 1>( delim, kvDelim, oss, std::forward<Args>( args )... );
    }
    return oss;
}

template<size_t idx = 0, class... Args>
std::ostream &printTuple( std::ostream &oss, const std::tuple<Args...> &val )
{
    if constexpr ( idx == 0 )
    {
        if ( !getGlobalDelimiters().tupleQuotes.empty() )
            oss << getGlobalDelimiters().tupleQuotes[0];
    }
    if constexpr ( idx == sizeof...( Args ) )
    {
        if ( !getGlobalDelimiters().tupleQuotes.empty() )
            oss << getGlobalDelimiters().tupleQuotes[1];
    }
    else
    {
        if constexpr ( idx > 0 )
            oss << getGlobalDelimiters().tupleDelim;
        print( oss, std::get<idx>( val ) );
        return printTuple<idx + 1>( oss, val );
    }
    return oss;
}

template<class Vec>
std::ostream &printVec( std::ostream &oss, const Vec &vec, bool bUseMapQuotes = false )
{
    auto quotes = bUseMapQuotes ? getGlobalDelimiters().mapQuotes : getGlobalDelimiters().vecQuotes;
    auto delim  = getGlobalDelimiters().elemDelim;
    if ( !quotes.empty() )
        oss << quotes[0];
    int i = 0;
    for ( const auto &e : vec )
    {
        if ( i++ )
            oss << delim;
        print( oss, e );
    }
    if ( !quotes.empty() )
        oss << quotes[1];
    return oss;
}

template<class Set>
std::ostream &printSet( std::ostream &oss, const Set &set )
{
    return printVec( oss, set, true );
}

template<class Vec>
std::ostream &printMap( std::ostream &oss, const Vec &vec )
{
    auto quotes  = getGlobalDelimiters().mapQuotes;
    auto delim   = getGlobalDelimiters().elemDelim;
    auto kvDelim = getGlobalDelimiters().pairDelim;
    if ( !quotes.empty() )
        oss << quotes[0];
    int i = 0;
    for ( const auto &e : vec )
    {
        if ( i++ )
            oss << delim;
        printPair( oss, e );
    }
    if ( !quotes.empty() )
        oss << quotes[1];
    return oss;
}

template<class K, class V>
std::ostream &operator<<( std::ostream &oss, const std::pair<K, V> &val )
{
    return printPair( oss, val );
}
template<class K, class V>
std::ostream &operator<<( std::ostream &oss, const std::tuple<K, V> &val )
{
    return printTuple( oss, val );
}
template<class... Args>
std::ostream &operator<<( std::ostream &oss, const std::vector<Args...> &val )
{
    return printVec( oss, val );
}
template<class T, size_t N>
std::ostream &operator<<( std::ostream &oss, const std::array<T, N> &val )
{
    return printVec( oss, val );
}
template<class... Args>
std::ostream &operator<<( std::ostream &oss, const std::map<Args...> &val )
{
    return printMap( oss, val );
}
template<class... Args>
std::ostream &operator<<( std::ostream &oss, const std::set<Args...> &val )
{
    return printSet( oss, val );
}
template<class... Args>
std::ostream &operator<<( std::ostream &oss, const std::unordered_map<Args...> &val )
{
    return printMap( oss, val );
}
template<class... Args>
std::ostream &operator<<( std::ostream &oss, const std::unordered_set<Args...> &val )
{
    return printSet( oss, val );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//               FormatB
//////////////////////////////////////////////////////////////////////////////////////////////////////////
struct FormatB
{
    template<class OStream>
    const FormatB &operator()( OStream &os, const char *fmt ) const
    {
        for ( ; *fmt; ++fmt )
        {
            if ( ( *fmt == '{' && *( fmt + 1 ) == '{' ) || ( *fmt == '}' && *( fmt + 1 ) == '}' ) ) // escape {{ and }}
                ++fmt;
            else
                os << *fmt;
        }
        return *this;
    }

    template<class OStream, class A, class... Args>
    const FormatB &operator()( OStream &os, const char *fmt, A &&a, Args &&...args ) const
    {
        for ( ; *fmt; ++fmt )
        {
            if ( *fmt == '{' && *( fmt + 1 ) == '}' )
            {
                os << a; //derefSmartPtr( a );
                return operator()( os, fmt + 2, std::forward<Args>( args )... );
            }
            if ( ( *fmt == '{' && *( fmt + 1 ) == '{' ) || ( *fmt == '}' && *( fmt + 1 ) == '}' ) ) // escape {{ and }}
                ++fmt;
            else
                os << *fmt;
        }
        // now fmt is empty, output rest args.
        os << a;
        return operator()( os, fmt, std::forward<Args>( args )... );
    }

    template<class OStream>
    const FormatB &endl( OStream &os ) const
    {
        os << std::endl;
        return *this;
    }
};
static constexpr FormatB formatb;

template<class... Args>
std::string format_to_str( const char *fmt, Args &&...args )
{
    std::stringstream ss;
    formatb( ss, fmt, std::forward<Args>( args )... );
    return ss.str();
}

/// Print timestamp.
/// @param time if ts.tv_nsec == -1, get local time.
/// @param buffer [out] if it's nullptr, use internal static buffer.
/// @param subsecondDigits can only be 0, 3, 6, or 9
inline const char *print_time(
        char *buffer = nullptr, timespec ts = { -1, -1 }, int subsecondDigits = 6, bool bUTCTime = false, const char *timeFmt = "%Y%m%d-%T" )
{
    static const unsigned BUFFERSIZE = 32;
    static char           localbuf[BUFFERSIZE];
    const char           *subsecFmt[3] = { ".%03d", ".%06d", ".%069" };
    assert( subsecondDigits == 0 || subsecondDigits == 3 || subsecondDigits == 6 || subsecondDigits == 9 );

    char *buf = buffer ? buffer : localbuf;
    if ( ts.tv_nsec == -1 )
        clock_gettime( CLOCK_REALTIME, &ts );
    time_t timet = ts.tv_sec;
    tm     atime;
    bUTCTime ? gmtime_r( &timet, &atime ) : localtime_r( &timet, &atime );
    size_t nbytes = strftime( buf, BUFFERSIZE, timeFmt, &atime );                              // YYYYMMDD-HH:MM:SS
    if ( subsecondDigits > 0 && subsecondDigits < 10 )
        sprintf( buf + nbytes, subsecFmt[subsecondDigits / 3 - 1], int( ts.tv_nsec / 1000 ) ); // us
    return buf;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//               ProcOStream, SetLogLevelThreshold, jzlog
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// ofstream for process
class ProcOStream
{
public:
    ProcOStream( std::ostream *pOverride ) : _oss( pOverride ? pOverride : &_file )
    {
        if ( !pOverride )
        {
            openStream( program_invocation_short_name );
        }
    }
    ~ProcOStream()
    {
        _file << print_time() << " Process exiting." << std::endl;
    }

    /// @param overrideStream the new value if it's non-null
    static inline std::ostream &getGlobalProcOut( std::ostream *overrideStream = nullptr )
    {
        static ProcOStream procout{ overrideStream }; // static instance
        if ( overrideStream && overrideStream != procout._oss )
        {
            *procout._oss << print_time() << " This log ostream is overridden by new one." << std::endl;
            *overrideStream << print_time() << " This log ostream overrides old one." << std::endl;
            procout._oss = overrideStream;
        }
        return *procout._oss;
    }

private:
    void openStream( std::string prefix )
    {
        std::string name = prefix + "-pid" + std::to_string( getpid() ) + "-";
        name += print_time( nullptr,
                            {
                                    -1,
                                    -1,
                            },
                            0,
                            false,
                            "%Y%m%d_%H%M%S" ) +
                std::string( ".log" );
        _file.open( name, std::ios::app | std::ios::out );
        if ( !_file.is_open() )
        {
            std::cerr << print_time() << "Failed ot open log file:" << name << std::endl;
        }
        else
        {
            _file << print_time() << " Started logging for process pid:" << getpid() << std::endl;
        }
    }
    std::ofstream _file;
    std::ostream *_oss; // non-null
};

inline int LogLevelToInt( char level )
{
    switch ( level )
    {
    case 'E':
        return 1;
    case 'W':
        return 2;
    case 'I':
        return 3;
    case 'T':
        return 4;
    case 'D':
        return 0; // always log DEBUG in debug mode.
    default:
        assert( false && "Unknown log level" );
        return 0;
    }
}

/// Set the log level with a letter which is the first letter of log level: Error, WARN, Info, Trace, Debug
/// @level char type level. If it's invalid, it will not be set.
/// @return the integer representation of level.
inline int SetLogLevelThreshold( char level )
{
    static char s_jzLogLevelThreshold = 3; // INFO
    int         v                     = 0;
    if ( level && ( v = LogLevelToInt( level ) ) )
        s_jzLogLevelThreshold = v;
    return s_jzLogLevelThreshold;
}
inline bool canlog( char category )
{
    return LogLevelToInt( category ) <= SetLogLevelThreshold( 0 );
}

inline int &LogErrorCounter()
{
    static int s_counter = 0;
    return s_counter;
}

/// @param category in format "E", "W", "I", "T", "D". The letter represents log level: Error, WARN, Info, Trace, Debug
/// @param fmt  Format string. braces "{}" represent an argument.
template<class... Args>
void jzlogB( std::string_view category, const char *file, int line, std::ostream &oss, const char *fmt, Args &&...args )
{
    assert( category.size() >= 1 );
    char cat = category[0];
    assert( canlog( cat ) && "call canlog before calling jzlog to avoid unnecessary args construction." );
    if ( cat == 'E' )
        ++LogErrorCounter();
    if ( file )
        jz::formatb( oss, "{} {} [{}:{}] ", category, jz::print_time(), file, line );
    else
        jz::formatb( oss, "{} {} ", category, jz::print_time() );
    jz::formatb( oss, fmt, std::forward<Args>( args )... );
    oss << std::endl;
}

template<class... Args>
void jzlogB( std::string_view category, const char *file, int line, const char *fmt, Args &&...args )
{
#ifdef JZLOG_TO_STREAM
    std::ostream &oss = JZLOG_TO_STREAM;
#else
    std::ostream &oss = ProcOStream::getGlobalProcOut();
#endif
    jzlogB( category, file, line, oss, fmt, std::forward<Args>( args )... );
}

/// Log arguments as key-value pairs. It could be refactored to a structured logger.
/// @param category in format "E", "W", "I", "T", "D". The letter represents log level: Error, WARN, Info, Trace, Debug
/// @param desc the description text.
template<class... KVPairs>
void jzlogKV( std::string_view category, const char *file, int line, std::ostream &oss, const char *desc, KVPairs &&...kvpairs )
{
    static_assert( ( sizeof...( KVPairs ) % 2 ) == 0, "kvpairs must be like: k1, v1, k2, v2..." );

    assert( category.size() >= 1 );
    char cat = category[0];
    assert( canlog( cat ) && "call canlog before calling jzlog to avoid unnecessary args construction." );
    if ( cat == 'E' )
        ++LogErrorCounter();
    if ( file )
        jz::formatb( oss, "{} {} [{}:{}] {} ", category, jz::print_time(), file, line, desc );
    else
        jz::formatb( oss, "{} {} {} ", category, jz::print_time(), desc );
    if constexpr ( sizeof...( KVPairs ) > 0 )
    {
        oss << "{ ";
        printPairs( getGlobalDelimiters().elemDelim, getGlobalDelimiters().pairDelim, oss, std::forward<KVPairs>( kvpairs )... );
        oss << " }";
    }
    oss << std::endl;
}

template<class... KVPairs>
void jzlogKV( std::string_view category, const char *file, int line, const char *desc, KVPairs &&...kvpairs )
{
#ifdef JZLOG_TO_STREAM
    std::ostream &oss = JZLOG_TO_STREAM;
#else
    std::ostream &oss = ProcOStream::getGlobalProcOut();
#endif
    jzlogKV( category, file, line, oss, desc, std::forward<KVPairs>( kvpairs )... );
}

} // namespace jz
